from configparser import ConfigParser

import psycopg2

db_config_file = 'connector/database.ini'


class DataBase:
    conn = None
    is_connect = False

    def __int__(self, is_connect=False, conn=None):
        self.is_connect = is_connect
        self.configure()
        cursor, conn = self.connect()
        self.conn = conn
        self.cursor = cursor
        print("init")

    @staticmethod
    def configure(filename=db_config_file, section='postgresql'):
        # create a parser
        parser = ConfigParser()
        # read config file
        parser.read(filename)

        # get section, default to postgresql
        db = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                db[param[0]] = param[1]
        else:
            raise Exception('Section {0} not found in the {1} file'.format(section, filename))

        return db

    def close(self):
        con = self.conn
        try:
            if con is not None:
                con.close()
                self.is_connect = False
        finally:
            print('Database connection closed.')

    def connect(self):
        """ Connect to the PostgreSQL database server """
        try:
            # read connection parameters
            params = self.configure()

            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            self.conn = psycopg2.connect(**params)

            # create a cursor
            cursor = self.conn.cursor()

            # execute a statement
            # print('PostgreSQL database version:')
            cursor.execute('SELECT version()')

            # display the PostgreSQL database server version
            db_version = cursor.fetchone()
            # print(db_version)

            self.is_connect = True
            return cursor, self.conn

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            return None

    def connect_now(self):
        try:
            if self.cursor is not None:
                print("conn")
                sql = "select * from province;"
                self.cursor.execute(sql)
                print("Record yes")
                self.conn.commit()
            else:
                print("null conn")
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            self.conn.rollback()

    def insert_cities(self, table, value):
        try:
            if self.cursor is not None:
                sql = "insert into " + table + " (code, name, province) values " + value + ";"
                self.cursor.execute(sql)
                print("Record inserted")
                self.conn.commit()

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            self.conn.rollback()

    def select_cities(self):
        try:
            if self.cursor is not None:
                sql = "select * from city;"
                self.cursor.execute(sql)
                return self.cursor.fetchall()

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            self.conn.rollback()

    def insert_postcodes(self, table, value):
        try:
            # cursor, self.conn = self.connect()
            if self.cursor is not None:
                sql = "insert into " + table + " (code, location, city) values " + value + ";"
                self.cursor.execute(sql)
                print("Record inserted")
                self.conn.commit()
            else:
                print("null conn")
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            self.conn.rollback()


# TEST


if __name__ == '__main__':
    dbase = DataBase()
    # db.connect()
    # dbase.insert_cities('city', "('AB', 'Aberdeen', 'SCT')")
    # dbase.select()
    # dbase.set_state('b2', 'FETCHING')
