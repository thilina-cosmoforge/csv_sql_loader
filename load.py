import os
import time

import pandas as pd

import connector.postgres as pg

db = pg.DataBase()
db.__int__()
csv_file_path = 'D:/COSMOFRGE/2 - ATP/UK POST CODES/csv/'


def print_elapsed_time(start, end):
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)
    out = "{:0>2}h : {:0>2}m : {:05.2f}s".format(int(hours), int(minutes), seconds)
    print("Elapsed time: " + out)


def load_cities(file):
    data_frame = pd.read_csv(file)
    data = data_frame.astype({'code': 'str', 'name': 'str', 'province': 'str'})
    for i in range(len(data)):
        y = tuple(data.iloc[i])
        print(y)
        db.insert_cities('city', str(y))


def load_postcodes(file, city_id):
    data_frame = pd.read_csv(file)
    data_frame = pd.DataFrame(data_frame, columns=['Postcode', 'Latitude', 'Longitude'])
    data = data_frame.astype({'Postcode': 'str', 'Latitude': 'str', 'Longitude': 'str'})
    data['location'] = "POINT(" + data['Longitude'].astype(str) + " " + data['Latitude'].astype(str) + ")"
    data['city'] = city_id
    data = pd.DataFrame(data, columns=['Postcode', 'location', 'city'])
    # print(data)
    for i in range(len(data)):
        a = tuple(data.iloc[i])
        print(a)
        db.insert_postcodes('postcode', str(a))


if __name__ == '__main__':
    start_time = time.time()

    print(csv_file_path + 'AB postcodes.csv', 1)
    load_cities('csv/city.csv')
    # load_postcodes(csv_file_path + 'AB postcodes.csv', 1)

    # ===========================================================================================
    cities = []
    csv_files = []

    city_codes = ['']
    for x in db.select_cities():
        city_codes.append(x[1])
        cities.append(x)

    files = ['']
    for x in os.listdir(csv_file_path):
        if x.endswith(".csv"):
            # list csv files
            files.append(x.split(' ')[0])
            csv_files.append(x)

    res = [x for x in city_codes + files if x not in city_codes or x not in files]
    print(res)

    if not res:
        print("All PostCodes Available.")
        for x in cities:
            # print(csv_file_path + x[1] + ' postcodes.csv')
            # print(x[0])
            load_postcodes(csv_file_path + x[1] + ' postcodes.csv', int(x[0]))

    # ===========================================================================================
    db.close()
    end_time = time.time()
    print_elapsed_time(start_time, end_time)
